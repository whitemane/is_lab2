﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS_lab2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnMessageChoose_Click(object sender, EventArgs e)
        {
            this.tbMessage.Text = this.ChooseFilePath();
        }

        private void btnFirstKeyChoose_Click(object sender, EventArgs e)
        {
            this.tbFirstKey.Text = this.ChooseFilePath();
        }

        private void btnSecondKeyChoose_Click(object sender, EventArgs e)
        {
            this.tbSecondKey.Text = this.ChooseFilePath();
        }

        private void btnOutputFileChoose_Click(object sender, EventArgs e)
        {
            this.tbOutputFile.Text = this.ChooseFilePath();
        }

        private string ChooseFilePath()
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                return this.openFileDialog.FileName;
            }
            else
            {
                return string.Empty;
            }
        }

        private void btnEcrypt_Click(object sender, EventArgs e)
        {

        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {

        }
    }
}
