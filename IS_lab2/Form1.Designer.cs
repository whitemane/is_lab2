﻿namespace IS_lab2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tbMessage = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMessageChoose = new System.Windows.Forms.Button();
            this.btnSecondKeyChoose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSecondKey = new System.Windows.Forms.TextBox();
            this.btnFirstKeyChoose = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFirstKey = new System.Windows.Forms.TextBox();
            this.btnOutputFileChoose = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbOutputFile = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnEcrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(104, 12);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(255, 20);
            this.tbMessage.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Path to message";
            // 
            // btnMessageChoose
            // 
            this.btnMessageChoose.Location = new System.Drawing.Point(365, 10);
            this.btnMessageChoose.Name = "btnMessageChoose";
            this.btnMessageChoose.Size = new System.Drawing.Size(75, 23);
            this.btnMessageChoose.TabIndex = 2;
            this.btnMessageChoose.Text = "Choose";
            this.btnMessageChoose.UseVisualStyleBackColor = true;
            this.btnMessageChoose.Click += new System.EventHandler(this.btnMessageChoose_Click);
            // 
            // btnSecondKeyChoose
            // 
            this.btnSecondKeyChoose.Location = new System.Drawing.Point(365, 61);
            this.btnSecondKeyChoose.Name = "btnSecondKeyChoose";
            this.btnSecondKeyChoose.Size = new System.Drawing.Size(75, 23);
            this.btnSecondKeyChoose.TabIndex = 5;
            this.btnSecondKeyChoose.Text = "Choose";
            this.btnSecondKeyChoose.UseVisualStyleBackColor = true;
            this.btnSecondKeyChoose.Click += new System.EventHandler(this.btnSecondKeyChoose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Path to second key";
            // 
            // tbSecondKey
            // 
            this.tbSecondKey.Location = new System.Drawing.Point(117, 63);
            this.tbSecondKey.Name = "tbSecondKey";
            this.tbSecondKey.Size = new System.Drawing.Size(242, 20);
            this.tbSecondKey.TabIndex = 3;
            // 
            // btnFirstKeyChoose
            // 
            this.btnFirstKeyChoose.Location = new System.Drawing.Point(365, 36);
            this.btnFirstKeyChoose.Name = "btnFirstKeyChoose";
            this.btnFirstKeyChoose.Size = new System.Drawing.Size(75, 23);
            this.btnFirstKeyChoose.TabIndex = 8;
            this.btnFirstKeyChoose.Text = "Choose";
            this.btnFirstKeyChoose.UseVisualStyleBackColor = true;
            this.btnFirstKeyChoose.Click += new System.EventHandler(this.btnFirstKeyChoose_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Path to first key";
            // 
            // tbFirstKey
            // 
            this.tbFirstKey.Location = new System.Drawing.Point(104, 38);
            this.tbFirstKey.Name = "tbFirstKey";
            this.tbFirstKey.Size = new System.Drawing.Size(255, 20);
            this.tbFirstKey.TabIndex = 6;
            // 
            // btnOutputFileChoose
            // 
            this.btnOutputFileChoose.Location = new System.Drawing.Point(365, 86);
            this.btnOutputFileChoose.Name = "btnOutputFileChoose";
            this.btnOutputFileChoose.Size = new System.Drawing.Size(75, 23);
            this.btnOutputFileChoose.TabIndex = 11;
            this.btnOutputFileChoose.Text = "Choose";
            this.btnOutputFileChoose.UseVisualStyleBackColor = true;
            this.btnOutputFileChoose.Click += new System.EventHandler(this.btnOutputFileChoose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Path to output file";
            // 
            // tbOutputFile
            // 
            this.tbOutputFile.Location = new System.Drawing.Point(117, 88);
            this.tbOutputFile.Name = "tbOutputFile";
            this.tbOutputFile.Size = new System.Drawing.Size(242, 20);
            this.tbOutputFile.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(365, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Choose";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Path to encrypted";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(117, 112);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(242, 20);
            this.textBox1.TabIndex = 12;
            // 
            // btnEcrypt
            // 
            this.btnEcrypt.Location = new System.Drawing.Point(12, 137);
            this.btnEcrypt.Name = "btnEcrypt";
            this.btnEcrypt.Size = new System.Drawing.Size(206, 23);
            this.btnEcrypt.TabIndex = 15;
            this.btnEcrypt.Text = "Encrypt";
            this.btnEcrypt.UseVisualStyleBackColor = true;
            this.btnEcrypt.Click += new System.EventHandler(this.btnEcrypt_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(224, 138);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(206, 23);
            this.btnDecrypt.TabIndex = 15;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 172);
            this.Controls.Add(this.btnDecrypt);
            this.Controls.Add(this.btnEcrypt);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnOutputFileChoose);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbOutputFile);
            this.Controls.Add(this.btnFirstKeyChoose);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbFirstKey);
            this.Controls.Add(this.btnSecondKeyChoose);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbSecondKey);
            this.Controls.Add(this.btnMessageChoose);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbMessage);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox tbMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMessageChoose;
        private System.Windows.Forms.Button btnSecondKeyChoose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSecondKey;
        private System.Windows.Forms.Button btnFirstKeyChoose;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFirstKey;
        private System.Windows.Forms.Button btnOutputFileChoose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbOutputFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnEcrypt;
        private System.Windows.Forms.Button btnDecrypt;
    }
}

